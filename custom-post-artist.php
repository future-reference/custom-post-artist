<?php

/**
 * @wordpress-plugin
 * Plugin Name:       Custom Post Type - Artist
 * Plugin URI:        https://gitlab.com/future-reference/custom-post-artist
 * Description:       Wordpress Plugin to create a custom post type named artist
 * Version:           1.0.0
 * Author:            Future Reference
 * Author URI:        http://futurereference.co/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       custom-post-artist
 */

add_action('init', 'artist_register_post_types');

function artist_register_post_types()
{

    /* Register the Artist post type. */

    register_post_type(
        'artist',
        array(
            'description'         => '',
            'public'              => true,
            'publicly_queryable'  => true,
            'show_in_nav_menus'   => false,
            'show_in_admin_bar'   => true,
            'exclude_from_search' => false,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 20,
            'menu_icon'           => 'dashicons-art',
            'can_export'          => true,
            'delete_with_user'    => false,
            'hierarchical'        => false,
            'has_archive'         => true,
            'capability_type'     => 'post',

            /* The rewrite handles the URL structure. */
            'rewrite' => array(
                'slug'       => 'artist',
                'with_front' => false,
                'pages'      => true,
                'feeds'      => true,
                'ep_mask'    => EP_PERMALINK,
            ),

            /* What features the post type supports. */
            'supports' => array(
                'title',
                'editor',
                'revisions',
                'thumbnail'
            ),

            /* Labels used when displaying the posts. */
            'labels' => array(
                'name'               => __('Artists',                    'futurereference'),
                'singular_name'      => __('Artist',                     'futurereference'),
                'menu_name'          => __('Artists',                    'futurereference'),
                'name_admin_bar'     => __('Artists',                    'futurereference'),
                'add_new'            => __('Add New',                    'futurereference'),
                'add_new_item'       => __('Add New Artist',             'futurereference'),
                'edit_item'          => __('Edit Artist',                'futurereference'),
                'new_item'           => __('New Artist',                 'futurereference'),
                'view_item'          => __('View Artist',                'futurereference'),
                'search_items'       => __('Search Artists',             'futurereference'),
                'not_found'          => __('No artists found',           'futurereference'),
                'not_found_in_trash' => __('No artists found in trash',  'futurereference'),
                'all_items'          => __('Artists',                    'futurereference'),
            )
        )
    );
}
